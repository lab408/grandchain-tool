### What is this repository for? ###

grandchain.c為負責記錄各鏈狀況的OurContract，把當作GrandChain的OurChain跑起來後使用bitcoin-cli deploycontract [grandchain.c的路徑] 來布署這個合約到鏈上。這個合約會在執行bitcoind的資料夾下創建sideChains.txt，之後會把各鏈的狀態記錄到此檔案裡面

grandchain.c的功能：

>bitcoin-cli callcontract [grandchain.c’s ctid] join [ChainName] [ChainState] [UpdateInterval]

一個從來沒加入過GrandChain Protocol的區塊鏈可以用這個功能加入，[ChainName]為這條區塊鏈的名稱，不能和其他鏈重複，否則會加入失敗
[ChainState]為這條鏈目前最新的狀態，通常是儲存目前最新的blockhash
[UpdateInterval]為這條鏈欲更新狀態的間隔時間

>bitcoin-cli callcontract [grandchain.c’s ctid] update [ChainName] [ChainState]

如果一條鏈已經加入GrandChain Protocol，可以用這個功能更新狀態

>bitcoin-cli callcontract [grandchain.c’s ctid] change_update_interval [ChainName] [UpdateInterval]

如果一條鏈已經加入GrandChain Protocol，可以用這個功能修改欲更新狀態的間隔時間

=======================================

crosschain.cpp的功能：
這個程式的功能為讓區塊鏈的主鏈變更到在GrandChain 記錄的blockhash (記錄在上述的sideChains.txt，且此時的[ChainState]一定要記錄blockhash)
使用方法

>g++ crosschain.cpp –o crosschain

>./crosschain [ChainName] [sideChains.txt’s path] [bitcoin-cli …]

這個程式就會去找尋sideChains.txt中這個[ChainName]記錄的blockhash，用bitcoin-cli invalidateblock和reconsiderblock讓區塊鏈的主鏈變更這個區塊上
其中[bitcoin-cli …]要輸入這台電腦呼叫這條區塊鏈rpc的方式，例如這台電腦用bitcoin-cli –rpcpassword=123 –rpcport=123 generate 1呼叫這條區塊鏈的挖礦rpc，那[bitcoin-cli …]就要輸入bitcoin-cli –rpcpassword=123 –rpcport=123

