#include <ourcontract.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct sideChain{
   char chainName[20];
   char blockHash[200];
   char interval[10];
};

static int updateChain(const char *name, const char *hash)
{

    struct sideChain sideChains[100];
    FILE *readptr;
    int ind = 0;
    

    readptr = fopen("sideChains.txt","r");
    if (readptr == NULL)
        exit(EXIT_FAILURE);
    while (fscanf(readptr, "%s %s %s", sideChains[ind].chainName, sideChains[ind].blockHash, sideChains[ind].interval)!=EOF) {
        //printf("name: %s, hash: %s", sideChains[ind].chainName, sideChains[ind].blockHash);
        ind++;
    }

    int exist = 0;
    for(int i=0;i<ind;i++) {
        if(!strcmp(sideChains[i].chainName,name)) {
            sprintf(sideChains[i].blockHash, "%s", hash);
            exist++;
        }
    }

    if(exist==0) return 1;

   FILE *writeptr;
   // use appropriate location if you are using MacOS or Linux
   writeptr = fopen("sideChains.txt","w");
   if(writeptr == NULL)
        exit(EXIT_FAILURE);

    for(int i=0; i<ind;i++) {
        fprintf(writeptr, "%s %s %s\n",sideChains[i].chainName, sideChains[i].blockHash, sideChains[i].interval);
    }
   fclose(writeptr);

    return 0;
}

static int changeIntervalChain(const char *name, const char *interv)
{

    struct sideChain sideChains[100];
    FILE *readptr;
    int ind = 0;
    

    readptr = fopen("sideChains.txt","r");
    if (readptr == NULL)
        exit(EXIT_FAILURE);
    while (fscanf(readptr, "%s %s %s", sideChains[ind].chainName, sideChains[ind].blockHash, sideChains[ind].interval)!=EOF) {
        //printf("name: %s, hash: %s", sideChains[ind].chainName, sideChains[ind].blockHash);
        ind++;
    }

    int exist = 0;
    for(int i=0;i<ind;i++) {
        if(!strcmp(sideChains[i].chainName,name)) {
            sprintf(sideChains[i].interval, "%s", interv);
            exist++;
        }
    }

    if(exist==0) return 1;

   FILE *writeptr;
   // use appropriate location if you are using MacOS or Linux
   writeptr = fopen("sideChains.txt","w");
   if(writeptr == NULL)
        exit(EXIT_FAILURE);

    for(int i=0; i<ind;i++) {
        fprintf(writeptr, "%s %s %s\n",sideChains[i].chainName, sideChains[i].blockHash, sideChains[i].interval);
    }
   fclose(writeptr);

    return 0;
}

static int joinChain(const char *name, const char *hash, const char *interv)
{

    struct sideChain sideChains[100];
    FILE *readptr;
    int ind = 0;
    

    readptr = fopen("sideChains.txt","r");
    if (readptr == NULL)
        exit(EXIT_FAILURE);
    while (fscanf(readptr, "%s %s %s", sideChains[ind].chainName, sideChains[ind].blockHash, sideChains[ind].interval)!=EOF) {
        //printf("name: %s, hash: %s", sideChains[ind].chainName, sideChains[ind].blockHash);
        ind++;
    }

    for(int i=0;i<ind;i++) {
        if(!strcmp(sideChains[i].chainName,name)) {
            return 1;
        }
    }

    sprintf(sideChains[ind].chainName, "%s", name);
    sprintf(sideChains[ind].blockHash, "%s", hash);
    sprintf(sideChains[ind].interval, "%s", interv);
    ind++;

   FILE *writeptr;
   // use appropriate location if you are using MacOS or Linux
   writeptr = fopen("sideChains.txt","w");
   if(writeptr == NULL)
        exit(EXIT_FAILURE);

    for(int i=0; i<ind;i++) {
        fprintf(writeptr, "%s %s %s\n",sideChains[i].chainName, sideChains[i].blockHash, sideChains[i].interval);
    }
   fclose(writeptr);

    return 0;
}

int contract_main(int argc, char **argv)
{

    if (argc < 2) {
        err_printf("%s: no subcommand\n", argv[0]);
        FILE *writeptr;
        // use appropriate location if you are using MacOS or Linux
        writeptr = fopen("sideChains.txt","w");
        if(writeptr == NULL)
            exit(EXIT_FAILURE);
        fclose(writeptr);
        return 0;
    }

    /* subcommand */
    if (str_cmp(argv[1], "update", 6) == 0) {
        if (argc != 4) {
            err_printf("%s: usage: update chainName blockHash\n", argv[0]);
            return 0;
        }

        int ret = updateChain(argv[2], argv[3]);
        if (ret != 0) {
            err_printf("%s: update failed\n", argv[0]);
            return 0;
        }
        return 0;
    }
    else if (str_cmp(argv[1], "join", 4) == 0) {
        if (argc != 5) {
            err_printf("%s: usage: join chainName blockHash updateInterval\n", argv[0]);
            return 0;
        }

        int ret = joinChain(argv[2], argv[3], argv[4]);
        if (ret != 0) {
            err_printf("%s: join failed\n", argv[0]);
            return 0;
        }
        return 0;
    }
    else if (str_cmp(argv[1], "change_update_interval", 22) == 0) {
        if (argc != 4) {
            err_printf("%s: usage: change_update_interval chainName updateInterval\n", argv[0]);
            return 0;
        }

        int ret = changeIntervalChain(argv[2], argv[3]);
        if (ret != 0) {
            err_printf("%s: change_update_interval failed\n", argv[0]);
            return 0;
        }
        return 0;
    }


    return 0;
}
